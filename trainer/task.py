
"""Executes model training and evaluation."""

import argparse
import logging
import os
import sys

import hypertune
import numpy as np
from sklearn import model_selection
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC

from trainer import metadata
from trainer import utils

from stop_words import get_stop_words


def _train_and_evaluate(dataset, output_dir):

    stop_words = get_stop_words('french')

    X_train, X_test, y_train, y_test = train_test_split(
        dataset['verbatim'],
        dataset['category'],
        random_state=0,
        test_size=0.1
    )

    pipeline = Pipeline([
        ('tfidf', TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='latin-1', ngram_range=(1, 2),
                                  stop_words=stop_words)),
        ('SVC', LinearSVC())
    ])

    parameters = {'tfidf__ngram_range': [(1, 1), (1, 2)],
                  'tfidf__use_idf': (True, False),
                  'tfidf__max_df': [0.25, 0.5, 0.75, 1.0],
                  'tfidf__max_features': [10, 50, 100, 250, 500, 1000, None],
                  'tfidf__stop_words': (stop_words, None),
                  'tfidf__smooth_idf': (True, False),
                  'tfidf__norm': ('l1', 'l2', None),
                  }

    grid = GridSearchCV(pipeline, parameters, cv=3, verbose=1, n_jobs=-1)
    grid.fit(X_train, y_train)

    bestlinearSVC = grid.best_estimator_
    bestlinearSVC.fit(X_train, y_train)
    bestlinearSVC.coef_ = bestlinearSVC.named_steps['SVC'].coef_
    bestlinearSVC.score(X_train, y_train)

    bestlinearSVC.score(X_test, y_test)

    bestlinearSVC.predict(X_test)

    scores = model_selection.cross_val_score(bestlinearSVC, X_test, y_test, cv=3)

    logging.info(scores)

    # Write model and eval metrics to `output_dir`
    model_output_path = os.path.join(
        output_dir, 'model', metadata.MODEL_FILE_NAME)

    metric_output_path = os.path.join(
        output_dir, 'experiment', metadata.METRIC_FILE_NAME)

    utils.dump_object(bestlinearSVC, model_output_path)
    utils.dump_object(scores, metric_output_path)

    hpt = hypertune.HyperTune()
    hpt.report_hyperparameter_tuning_metric(
        hyperparameter_metric_tag='my_metric_tag',
        metric_value=np.mean(scores),
        global_step=1000)


def run_experiment(flags):
    """Testbed for running model training and evaluation."""
    # Get data for training and evaluation

    dataset = utils.read_df_from_bigquery(
        flags.input, num_samples=flags.num_samples, project_id="insight-machine")

    # Get model
#    estimator = model.get_estimator(flags)

    # Run training and evaluation
    _train_and_evaluate(dataset, flags.job_dir)


def _parse_args(argv):
    """Parses command-line arguments."""

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--input',
        help='''Dataset to use for training and evaluation.
              Can be BigQuery table or a file (CSV).
              If BigQuery table, specify as as PROJECT_ID.DATASET.TABLE_NAME.
            ''',
        required=True,
    )

    parser.add_argument(
        '--job-dir',
        help='Output directory for exporting model and other metadata.',
        required=True,
    )

    parser.add_argument(
        '--log_level',
        help='Logging level.',
        choices=[
            'DEBUG',
            'ERROR',
            'FATAL',
            'INFO',
            'WARN',
        ],
        default='INFO',
    )

    parser.add_argument(
        '--num_samples',
        help='Number of samples to read from `input`',
        type=int,
        default=None,
    )

    parser.add_argument(
        '--n_estimators',
        help='Number of trees in the forest.',
        default=10,
        type=int,
    )

    parser.add_argument(
        '--max_depth',
        help='The maximum depth of the tree.',
        type=int,
        default=None,
    )

    parser.add_argument(
        '--min_samples_leaf',
        help='The minimum number of samples required to be at a leaf node.',
        default=1,
        type=int,
    )

    parser.add_argument(
        '--criterion',
        help='The function to measure the quality of a split.',
        choices=[
            'gini',
            'entropy',
        ],
        default='gini',
    )

    return parser.parse_args(argv)


def main():
    """Entry point."""

    flags = _parse_args(sys.argv[1:])
    logging.basicConfig(level=flags.log_level.upper())
    run_experiment(flags)


if __name__ == '__main__':
    main()
