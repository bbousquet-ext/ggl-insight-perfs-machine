resource "google_service_account" "composer-service-account" {
  project      = "${local.project_id}"
  account_id   = "composer-service-account"
  display_name = "composer-service-account"
}

resource "google_service_account" "composer-insight-machine" {
  project      = "${local.project_id}"
  account_id   = "composer-insight-machine"
  display_name = "composer-insight-machine"
}

resource "google_service_account" "ci-composer-account" {
  project      = "${local.project_id}"
  account_id   = "ci-composer-account"
  display_name = "ci-composer-account"
}
