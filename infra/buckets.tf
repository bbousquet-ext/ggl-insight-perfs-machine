# bucket to store dataflow templates, staging and tmp folders
resource "google_storage_bucket" "job-package" {
  project       = "${local.project_id}"
  name          = "${local.project_id}-job-package"
  storage_class = "REGIONAL"
  location      = "europe-west1"
}

resource "google_storage_bucket" "dataflow" {
  project       = "${local.project_id}"
  name          = "${local.project_id}-dataflow"
  storage_class = "REGIONAL"
  location      = "europe-west1"
}

resource "google_storage_bucket" "credentials-bucket" {
  project       = "${local.project_id}"
  name          = "${local.project_id}-credentials"
  storage_class = "REGIONAL"
  location      = "europe-west1"
}
