output "project" {
  value = "${local.project_id}"
}

output "composer-insight-machine-service-account-email" {
  value = "${google_service_account.composer-insight-machine}"
}

output "composer-service-account-email" {
  value = "${google_service_account.composer-service-account.email}"
}