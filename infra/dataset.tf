resource "google_bigquery_dataset" "temporary" {
  project                     = "${local.project_id}"
  dataset_id                  = "temporary"
  friendly_name               = "temporary"
  description                 = "temporary tables "
  location                    = "EU"
  default_table_expiration_ms = 43200000  #12h
}
