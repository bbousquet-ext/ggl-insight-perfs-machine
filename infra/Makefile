#Makefile for using terraform, Version: Fri Mar  1 14:47:51 CET 2019
export DEPLOY_PROJECT=gl-data-factory-deploy
export GOOGLE_CREDENTIALS ?= ~/.ssh/${DEPLOY_PROJECT}-terraform-service-account.json

ifdef CI
	export TERRAFORM_CMD = terraform
else
	export TERRAFORM_CMD = docker run -it --rm \
		-v `pwd`:/root/terraform \
		-v ${GOOGLE_CREDENTIALS}:/root/.ssh/sa-terraform-service-account \
		-e GOOGLE_CREDENTIALS=/root/.ssh/sa-terraform-service-account \
		-w /root/terraform \
		hashicorp/terraform:0.11.7
endif

# This Makefile wrapp all terraform commands via docker
# To run this commands you must have a json service account in 
# ~/.ssh/gl-data-factory-deploy-terraform-service-account.json
# Afer running init, choose the good workspace run plan and then apply 

workspaces = prod preprod recette

init:
	# first to sync local repo with bucket state
	@$(TERRAFORM_CMD) init \
	  -backend-config="project=$(DEPLOY_PROJECT)" \
	  -backend-config="bucket=$(DEPLOY_PROJECT)-terraform-state-bucket"

plan apply destroy import show help refresh taint:
	# commands with parameters
	@echo "Working on environment: $$(cat .terraform/environment)"
	@if [ -d "vars" ]; then \
		conf="-var-file=vars/$$(cat .terraform/environment).tfvars";\
		fi;\
	$(TERRAFORM_CMD) $@ \
		 $$conf -var "current-project=$(DEPLOY_PROJECT)" \
		$(filter-out $@,$(MAKECMDGOALS))

fmt:
	#to lint all files
	@$(TERRAFORM_CMD) fmt .

check :
	# to check if nothing has to be applied
	@if [ -d "vars" ]; then \
    		conf="-var-file=vars/$$(cat .terraform/environment).tfvars";\
    		fi;\
	$(TERRAFORM_CMD) plan -detailed-exitcode \
	$$conf -var "current-project=$(DEPLOY_PROJECT)"

auto_apply :
	# apply without approbation
	@if [ -d "vars" ]; then \
    		conf="-var-file=vars/$$(cat .terraform/environment).tfvars";\
    		fi;\
	$(TERRAFORM_CMD) apply -auto-approve \
	$$conf -var "current-project=$(DEPLOY_PROJECT)"

workspace state graph:
	@$(TERRAFORM_CMD) $@ $(filter-out $@,$(MAKECMDGOALS))

$(workspaces):
	# enter make prod to switch to prod environment
	@$(TERRAFORM_CMD) workspace select $@

auth:
	# get a key for service account
	- rm -r ~/.ssh/$(DEPLOY_PROJECT)-terraform-service-account.json
	gcloud iam service-accounts keys create  ~/.ssh/$(DEPLOY_PROJECT)-terraform-service-account.json\
	 --iam-account terraform-service-account@$(DEPLOY_PROJECT).iam.gserviceaccount.com
%:
	@:

usage:
	@printf  "$$(grep -e '#\|:$$'  Makefile  | grep -v '%' | sed 's/\(.*\):$$/\\033\[1m \1 \\033[0m/g')\n"


.DEFAULT_GOAL := usage
.PHONY: plan apply destroy state import show help refresh graph usage lint workspace usage 



