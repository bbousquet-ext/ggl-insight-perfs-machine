resource "google_composer_environment" "job-composer" {
  name = "job-composer-${terraform.workspace}"
  region = "europe-west1"
  project = "${local.project_id}"
  provider = "google-beta"

  config {
    node_count = 3

    node_config {
      zone            = "europe-west1-b"
      machine_type    = "n1-standard-1"
      service_account = "${google_service_account.composer-service-account.name}"
    }

    software_config {
      env_variables {
        env = "${terraform.workspace}"
        SENDGRID_MAIL_FROM = "zebaz_team@galerieslafayette.com"
        SENDGRID_API_KEY = "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"
      }

      image_version = "composer-1.6.1-airflow-1.10.1"
      python_version = 3
      airflow_config_overrides {
        core-parallelism = 5
        core-dags_are_paused_at_creation = "True"
        scheduler-catchup_by_default= "False"
      }
    }
  }

  depends_on = [
    "google_service_account.composer-service-account",
    "google_project_iam_binding.composer_worker_role",
  ]
}

