provider "google" {
  region  = "europe-west1"
  version = "~> 2.5.1"
}

provider "google-beta" {
  region = "europe-west1"
}
