# Remote state of base infrastructure
data "terraform_remote_state" "insight_machine_state" {
  backend   = "gcs"
  workspace = "prod"

  config {
    bucket = "ggl-insight-perfs-insight-machine-deploy-terraform-state-bucket"
  }
}
