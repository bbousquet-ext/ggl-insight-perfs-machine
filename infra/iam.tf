resource "google_project_iam_binding" "composer_worker_role" {
  project = "${local.project_id}"
  role    = "roles/composer.worker"

  members = [
    "serviceAccount:${google_service_account.composer-service-account.email}",
    "serviceAccount:${google_service_account.ci-composer-account.email}",
  ]
}

resource "google_project_iam_binding" "composer_bigquery_role" {
  project = "${local.project_id}"
  role    = "roles/bigquery.admin"

  members = [
    "serviceAccount:${google_service_account.composer-insight-machine.email}",
    "serviceAccount:${google_service_account.composer-service-account.email}",
  ]
}

resource "google_project_iam_binding" "composer_user_role" {
  project = "${local.project_id}"
  role    = "roles/composer.user"

  members = [
    "group:gcp_pem_respmkt@galerieslafayette.com",
    "group:gcp_ds4dmd_data@galerieslafayette.com",
    "group:gcp_zebaz_po@galerieslafayette.com",
    "serviceAccount:${google_service_account.ci-composer-account.email}",
  ]
}

resource "google_storage_bucket_iam_binding" "package_iam_binding" {
  bucket = "${google_storage_bucket.job-package.name}"
  role   = "roles/storage.objectViewer"

  members = [
    "serviceAccount:composer-service-account@gl-segmentations-cli-${terraform.workspace}.iam.gserviceaccount.com",
  ]
}

resource "google_storage_bucket_iam_binding" "dataflow_iam_binding" {
  bucket = "${google_storage_bucket.dataflow.name}"
  role = "roles/storage.objectViewer"

  members = [
    "serviceAccount:${google_service_account.composer-insight-machine.email}",
  ]
}

resource "google_project_iam_binding" "dataflow_worker_role" {
  project = "${local.project_id}"
  role = "roles/dataflow.worker"

  members = [
    "serviceAccount:${google_service_account.composer-insight-machine.email}",
  ]
}

resource "google_project_iam_binding" "dataflow_admin_role" {
  project = "${local.project_id}"
  role = "roles/dataflow.admin"

  members = [
    "serviceAccount:${google_service_account.composer-insight-machine.email}",
  ]
}

resource "google_project_iam_binding" "dataflow_service_account_role" {
  project = "${local.project_id}"
  role    = "roles/iam.serviceAccountUser"

  members = [
    "serviceAccount:${google_service_account.composer-insight-machine.email}",
  ]
}
