import json
import unittest


from orchestration.critizr.modules.utils import *


class TestFonctionUtils(unittest.TestCase):

    def test_get_schema_from_file(self):
        ### Given ###
        bucket_name = "machine-critizr"
        source_file = "schema_critzr_train_dataset.json"

        ### When ###
        element = get_schema_from_file(
            bucket_name=bucket_name,
            source_file=source_file
        )

        ### Then ###
        self.assertEqual(element, "test.csv")

    def test_get_csv_filename_from(self):
        ### Given ###
        source_file_name = "test.xlsx"

        ### When ###
        element = get_csv_filename_from(source_file_name)

        ### Then ###
        self.assertEqual(element, "test.csv")

    def test_get_schema_as_list(self):
        ### Given ###
        with open('schema_critzr_train_dataset.json', 'r') as myfile:
            data = myfile.read()

        schema_json = json.loads(data)

        ### When ###
        element = get_schema_as_list(schema_json)

        ### Then ###
        self.assertEqual(element, "category:STRING, verbatim:STRING, satisfaction_client:STRING, load_dt:STRING")

    def test_transform_csv_to_list(self):
        ### Given ###
        data = pd.read_csv("prediction_data.csv")

        ### When ###
        element = transform_csv_to_list(data, "verbatim")

        ### Then ###
        self.assertEqual(element[0], """Vs ne mavez pas propose demballage cadeau ni un choux dechantillons ni un libelle pour une csrte cadeau.
Mon achat etait pour un cadeau.
Or sur la plupart des sites, ces options st tjrs propisees.Jespere q ce sera bientot de das aux Galeries.
Merci davance""")


if __name__ == '__main__':
    unittest.main()