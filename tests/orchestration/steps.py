import unittest


from orchestration.critizr.modules.steps import get_schema_from_file


class TestFonctionSteps(unittest.TestCase):

    def preprocess_vecko_data(self):
        ### Given ###
        bucket_name = "machine-critizr"
        source_file = "schema_critzr_train_dataset.json"

        ### When ###
        element = get_schema_from_file(
            bucket_name=bucket_name,
            source_file=source_file
        )

        ### Then ###
        self.assertEqual(element, "test.csv")


if __name__ == '__main__':
    unittest.main()
