import json
from typing import Dict, List

import pandas as pd
from airflow.contrib.hooks import gcs_hook


def get_csv_filename_from(source_file: str) -> str:
    return source_file.replace('xlsx', 'csv')


def convert_xlsx_to_csv(bucket_name: str, source_file: str, **kwargs):
    """
    Convertit un fichier Excel depuis GCS en CSV au même endroit
    :param bucket_name: Le bucket de travail
    :param source_file: Préfixe du fichier source sur GCS
    :param kwargs: Interne
    :return: None
    """

    hook = gcs_hook.GoogleCloudStorageHook()

    # Bug de Composer sur l'import de gcfs. On utilise le hook à la place.
    hook.download(bucket=bucket_name,
                  object="data/{}".format(source_file),
                  filename="temp.xlsx"
    )

    excel_data = pd.read_excel("temp.xlsx")
    excel_data.to_csv('prediction_data.csv', index=False)

    hook.upload(bucket=bucket_name,
                object="data/" + get_csv_filename_from(source_file),
                filename='prediction_data.csv',
                mime_type='text/plain')


def get_schema_from_file(bucket_name: str, source_file: str):
    """
    Convertit un fichier Excel depuis GCS en CSV au même endroit
    :param bucket_name: Le bucket de travail
    :param source_file: Préfixe du fichier source sur GCS
    :param kwargs: Interne
    :return: None
    """

    with open(source_file, 'r') as myfile:
        data = myfile.read()

    return json.loads(data)


def get_schema_as_list(fields: List[Dict]) -> str:
    return_str = ""
    for field in fields:
        return_str = return_str + ", {}:{}".format(field['name'], field['type'])

    return return_str[2:]


def transform_csv_to_list(data, column_name: str):
    return data[column_name].values.tolist()


def extract_features_from_csv(bucket_name: str, source_file: str, column_name: str):

    hook = gcs_hook.GoogleCloudStorageHook()

    # Bug de Composer sur l'import de gcfs. On utilise le hook à la place.
    hook.download(bucket=bucket_name,
                  object=source_file,
                  filename="prediction_data.csv"
    )

    data = pd.read_csv("prediction_data.csv")

    return transform_csv_to_list(data, column_name)

