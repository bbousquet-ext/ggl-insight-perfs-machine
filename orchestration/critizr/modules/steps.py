import uuid

from googleapiclient import discovery
from airflow.contrib.operators import bigquery_to_gcs, dataflow_operator, gcs_to_bq
from airflow.operators import python_operator
from airflow.utils.trigger_rule import TriggerRule

from orchestration.critizr.modules.metadata import WORKING_BUCKET, BQ_DATASET_NAME, DS_TAG, DATAFLOW_FILE
from orchestration.critizr.modules.utils import convert_xlsx_to_csv, extract_features_from_csv
from orchestration.critizr.modules.variables import get_vecko_data_filename, variables, get_package_uris, train_dataset, \
    get_model_name
from airflow.contrib.operators import mlengine_operator


def convert_to_csv(step_name: str):
    return python_operator.PythonOperator(task_id=step_name,
                                          python_callable=convert_xlsx_to_csv,
                                          op_args=[
                                              WORKING_BUCKET,
                                              get_vecko_data_filename()
                                          ],
                                          provide_context=True,
                                          trigger_rule=TriggerRule.ALL_SUCCESS)


def load_gcs_to_bq(step_name: str, data_name: str, gcs_filename: str, skip_leading_rows: int = 0):
    return gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
        task_id=step_name,
        bucket=WORKING_BUCKET,
        source_objects=[gcs_filename],
        ignore_unknown_values=True,
        allow_quoted_newlines=True,
        write_disposition='WRITE_TRUNCATE',
        quote_character='"',
        allow_jagged_rows=True,
        skip_leading_rows=skip_leading_rows,
        schema_object="schemas/schema_{}.json".format(data_name),
        destination_project_dataset_table="{}.{}".format(BQ_DATASET_NAME, data_name)
    )


def export_bq_to_gcs(step_name: str, table_name: str, output_file_uri: str):
    return bigquery_to_gcs.BigQueryToCloudStorageOperator(
        task_id=step_name,
        source_project_dataset_table=table_name,
        destination_cloud_storage_uris=[output_file_uri]
    )


def preprocess_data(step_name: str, input_query: str, output_table: str, output_schema_file: str):

    # Args required for the Dataflow job.
    job_args = {
        'input': input_query,
        'output_table': output_table,
        'output_schema': output_schema_file,
        'load_dt': DS_TAG
    }

    return dataflow_operator.DataFlowPythonOperator(
        task_id=step_name,
        py_file=DATAFLOW_FILE,
        options=job_args)


def train_critizr_model(step_name: str):
    return mlengine_operator.MLEngineTrainingOperator(
        task_id=step_name,
        project_id=variables.get_project_id(),
        job_id="{}_{}".format(step_name, str(uuid.uuid4())),
        package_uris="gs://{}/artifacts/{}".format(WORKING_BUCKET, get_package_uris()),
        training_python_module='trainer.task',
        training_args=["--input", "{}.{}".format(variables.get_project_id(), train_dataset()), '--job-dir',
                       "gs://{}".format(WORKING_BUCKET)],
        region='europe-west1',
        scale_tier='STANDARD_1',
        runtime_version='1.13',
        python_version='3.5'
    )


def create_new_model_version(step_name: str, version: str):
    return mlengine_operator.MLEngineVersionOperator(
        task_id=step_name,
        project_id=variables.get_project_id(),
        model_name=get_model_name(),
        operation='create',
        version_name=version,
        version={
            'name': version,
            'deployment_uri': "gs://{}/model/".format(WORKING_BUCKET),
            'framework': 'SCIKIT_LEARN',
            'runtime_version': '1.13',
            'python_version': '3.5'
        }
    )


def make_predictions(step_name: str, input_file_name: str, output_file_name: str, version: str):
    return mlengine_operator.MLEngineBatchPredictionOperator(
        task_id=step_name,
        project_id=variables.get_project_id(),
        model_name=get_model_name(),
        region='europe-west1',
        input_paths=["gs://{}/data/{}.csv".format(WORKING_BUCKET, input_file_name)],
        output_path="gs://{}/data/{}.csv".format(WORKING_BUCKET, output_file_name),
        version_name=version,
        data_format="DATA_FORMAT_UNSPECIFIED",
        job_id="{}_{}".format(step_name, str(uuid.uuid4()))
    )


def predict(step_name: str, input_file_prefix: str, version: str = None):
    return python_operator.PythonOperator(task_id=step_name,
                                          python_callable=predict_json,
                                          op_args=[
                                              WORKING_BUCKET,
                                              input_file_prefix,
                                              version
                                          ],
                                          provide_context=True,
                                          trigger_rule=TriggerRule.ALL_SUCCESS)


def predict_json(bucket_name: str, input_file_prefix: str, version: str = None, **kwargs):
    service = discovery.build('ml', 'v1')
    name = 'projects/{}/models/{}'.format(variables.get_project_id(), get_model_name())

    if version is not None:
        name += '/versions/{}'.format(version)

    response = service.projects().predict(
        name=name,
        body={'instances': extract_features_from_csv(
            bucket_name=bucket_name,
            source_file=input_file_prefix,
            column_name="verbatim"
        )}
    ).execute()

    if 'error' in response:
        raise RuntimeError(response['error'])

    return response['predictions']
