
import datetime
import os

from airflow import configuration
from airflow import models

from orchestration.critizr.modules.variables import variables, MODULE_NAME, SOURCE_NAME

YESTERDAY = datetime.datetime.combine(
    datetime.datetime.today() - datetime.timedelta(1),
    datetime.datetime.min.time())

WORKING_BUCKET = "{}-{}".format(MODULE_NAME, SOURCE_NAME)
BQ_DATASET_NAME = "{}_{}".format(MODULE_NAME, SOURCE_NAME)

DS_TAG = '{{ ds }}'
DATAFLOW_FILE = os.path.join(
    configuration.get('core', 'dags_folder'),
    "dataflow/{}/{}".format(MODULE_NAME, SOURCE_NAME),
    "preprocess_critizr_data.py"
)

# The following additional Airflow variables should be set:
# gcp_project:         Google Cloud Platform project id.
# gcp_temp_location:   Google Cloud Storage location to use for Dataflow temp location.

DEFAULT_DAG_ARGS = {
    'start_date': YESTERDAY,
    'email': models.Variable.get('email'),
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 0,
    'project_id': variables.get_project_id(),
    'dataflow_default_options': {
        'project': variables.get_project_id(),
        'temp_location': variables.get_temp_location(),
        'runner': 'DataflowRunner',
        'region': 'europe-west1',
        'zone': 'europe-west1-b'
    }
}

SQL_QUERY_GET_VECKO_DATA_FOR_TRAINING = """
    SELECT 
        b.ce_que_disent_mes_clients as category, 
        verbatim, 
        satisfaction_client 
    FROM 
        `insight-machine.insight_machine.donnes_poc_vecko` a, 
        `insight-machine.insight_machine.donnes_poc_vecko_category` b 
    WHERE 
        a.ce_que_disent_mes_clients is not null and 
        a.ce_que_disent_mes_clients = b.category
"""


SQL_QUERY_GET_CRITIZR_DATA_FOR_PREDICTION = """
    SELECT 
        id, 
        content as verbatim 
    FROM 
        `insight-machine.critizr.critizr_table` a 
    WHERE 
        a.content is not null
"""