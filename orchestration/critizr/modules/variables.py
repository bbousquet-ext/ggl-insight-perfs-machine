from orchestration.common.variables import Variables

MODULE_NAME = 'machine'
SOURCE_NAME = 'critizr'

variables = Variables(
    module_name=MODULE_NAME,
    source_name=SOURCE_NAME
)


def get_vecko_data_filename():
    return variables.get_var('vecko_data').value


def get_output_schema():
    return variables.get_var('output_schema').value


def get_scale_tier():
    return variables.get_var('scale_tier').value


def get_package_uris():
    return variables.get_var('package_uris').value


def train_dataset():
    return variables.get_var('train_dataset').value


def get_model_name():
    return variables.get_var('model_name').value


def get_model_version_to_use():
    return variables.get_var('model_version_used').value


def get_model_version():
    return variables.get_var('model_version').value
