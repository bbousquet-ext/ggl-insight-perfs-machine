from airflow import models

from orchestration.critizr.modules.metadata import DEFAULT_DAG_ARGS, SQL_QUERY_GET_CRITIZR_DATA_FOR_PREDICTION

from orchestration.critizr.modules.steps import *
from orchestration.critizr.modules.variables import get_model_version_to_use, MODULE_NAME, SOURCE_NAME

DAG_ID = "{}-{}-predict-classification".format(MODULE_NAME, SOURCE_NAME)
DAG_DESCRIPTION = "Train prediction model for module {} with data source {}".format(MODULE_NAME, SOURCE_NAME)

with models.DAG(dag_id=DAG_ID,
                description=DAG_DESCRIPTION,
                schedule_interval=None,
                default_args=DEFAULT_DAG_ARGS
                ) as dag:

    prediction_table_name = "{}_{}.prediction_dataset".format(MODULE_NAME, SOURCE_NAME)
    prediction_file_prefix = "data/prediction_data.csv"
    prediction_file_uri = "gs://{}/{}".format(WORKING_BUCKET, prediction_file_prefix)

    # Launch the preprocessing of Critzr Data
    preprocess_data = preprocess_data(
        step_name="preprocess-critizr-data-for-prediction",
        input_query=SQL_QUERY_GET_CRITIZR_DATA_FOR_PREDICTION,
        output_table=prediction_table_name,
        output_schema_file="id:INTEGER, verbatim:STRING, load_dt:STRING"
    )

    # Convert Critizr Data to CSV
    export_to_csv = export_bq_to_gcs(
        step_name="export-critizr-data-to-csv",
        table_name="{}:{}".format(variables.get_project_id(), prediction_table_name),
        output_file_uri=prediction_file_uri
    )

    # make_predictions = make_predictions(
    #     step_name="predict-from-critizr-data",
    #     input_file_name=prediction_file_name,
    #     output_file_name="result_prediction_data",
    #     version=get_model_version_to_use()
    # )

    make_predictions = predict(
        step_name="predict-from-critizr-data",
        input_file_prefix=prediction_file_prefix,
        version=get_model_version_to_use()
    )

    preprocess_data >> export_to_csv >> make_predictions
