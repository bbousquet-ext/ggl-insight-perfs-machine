from airflow import models

from orchestration.critizr.modules.variables import get_model_version, MODULE_NAME, SOURCE_NAME
from orchestration.critizr.modules.metadata import DEFAULT_DAG_ARGS, SQL_QUERY_GET_VECKO_DATA_FOR_TRAINING
from orchestration.critizr.modules.utils import get_csv_filename_from

from orchestration.critizr.modules.steps import *

DAG_ID = "{}-{}-train-classification".format(MODULE_NAME, SOURCE_NAME)
DAG_DESCRIPTION = "Train classification model for module {} with data source {}".format(MODULE_NAME, SOURCE_NAME)

with models.DAG(dag_id=DAG_ID,
                description=DAG_DESCRIPTION,
                schedule_interval=None,
                default_args=DEFAULT_DAG_ARGS
                ) as dag:

    # Convert Excel Vecko Data to CSV
    convert_to_csv = convert_to_csv(
        step_name="convert-vecko-file-to-csv"
    )

    # Load Vecko CSV Data to BQ
    vecko_to_bigquery = load_gcs_to_bq(
         step_name="gcs-vecko-to-bigquery",
         data_name="vecko",
         gcs_filename="data/" + get_csv_filename_from(get_vecko_data_filename()),
         skip_leading_rows=1
    )

    # Load Vecko Category Mapping CSV Data to BQ
    vecko_category_to_bigquery = load_gcs_to_bq(
        step_name="gcs-vecko-category-to-bigquery",
        data_name="vecko_category",
        gcs_filename="data/vecko_top_problems.csv"
    )

    # Launch the preprocessing of Vecko Data
    preprocess_data = preprocess_data(
        step_name="preprocess-vecko-data-for-training",
        input_query=SQL_QUERY_GET_VECKO_DATA_FOR_TRAINING,
        output_table="{}_{}.train_dataset".format(MODULE_NAME, SOURCE_NAME),
        output_schema_file="category:STRING, verbatim:STRING, satisfaction_client:STRING, load_dt:STRING"
    )

    # Train model from dataset
    train_model = train_critizr_model("train_critizr_model")

    # Create a new version for the created model
    create_new_model_version = create_new_model_version(
        step_name="create-model-version",
        version=get_model_version()
    )

    convert_to_csv >> vecko_to_bigquery >> preprocess_data >> train_model >> create_new_model_version
    vecko_category_to_bigquery >> preprocess_data
