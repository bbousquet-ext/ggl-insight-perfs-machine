
from airflow import models


class Variable:

    def __init__(self, name: str, value: str):
        self.name = name
        self.value = value


class Variables:

    def __init__(self, module_name: str, source_name: str):
        self.module_name = module_name
        self.source_name = source_name
        self.variable = models.Variable

    def var_path(self, var_name: str):
        return "{}/{}/{}/{}".format(
            self.get_project_id(),
            self.module_name,
            self.source_name,
            var_name
        )

    def get_var(self, var_name: str):
        return Variable(
            name=var_name,
            value=self.variable.get(self.var_path(var_name))
        )

    def get_project_id(self):
        return self.variable.get('gcp_project')

    def get_temp_location(self):
        return self.variable.get('gcp_temp_location')
