from __future__ import absolute_import
import argparse
import logging
import re

import apache_beam as beam
from apache_beam.options import pipeline_options


class RowTransformer(object):

    def __init__(self, load_dt):
        self.load_dt = load_dt

    def parse(self, row):
        row['verbatim'] = row['verbatim'].replace('verbatim', '')\

        row['verbatim'] = re.sub(r'\S*@\S*\s?', "", row['verbatim'])

        row['load_dt'] = self.load_dt

        return row


def run(argv=None):
    """The main function which creates the pipeline and runs it."""
    parser = argparse.ArgumentParser()

    # Add the arguments needed for this specific Dataflow job.
    parser.add_argument(
        '--input', dest='input', required=True,
        help='Input data to read.  This can be a local file or '
             'a file in a Google Storage Bucket or a SQL query.')

    parser.add_argument('--output_table', dest='output_table', required=True,
                        help='Output BQ table to write results to.')

    parser.add_argument('--output_schema', dest='output_schema', required=False,
                        help='Output schema for output data table')

    parser.add_argument('--load_dt', dest='load_dt', required=True,
                        help='Load date in YYYY-MM-DD format.')

    known_args, pipeline_args = parser.parse_known_args(argv)
    row_transformer = RowTransformer(load_dt=known_args.load_dt)

    p_opts = pipeline_options.PipelineOptions(pipeline_args)

    with beam.Pipeline(options=p_opts) as pipeline:

        input_data = pipeline | 'ReadInputDataFromBQ' >> beam.io.Read(beam.io.BigQuerySource(
            query=known_args.input,
            use_standard_sql=True
        ))

        dict_records = input_data | "Clean row" >> beam.Map(
            lambda r: row_transformer.parse(r)
        )

        dict_records | "Write Output Dataset to BigQuery" >> beam.io.Write(
             beam.io.BigQuerySink(table=known_args.output_table,
                                  schema=known_args.output_schema,
                                  create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
                                  write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE))


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    run()
