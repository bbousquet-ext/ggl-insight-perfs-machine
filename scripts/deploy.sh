#!/usr/bin/env bash
CUR_DIR=$(pwd)
APP_NAME=PaymentService.jar
GCLOUD_PROJECT=gl-dsi-stab-exp-dev
BITBUCKET_REPO_SLUG=ggl-payment
BITBUCKET_COMMIT=1.0.3
LOG_LEVEL=info
DOCKER_IMAGE_NAME="eu.gcr.io/$GCLOUD_PROJECT/$BITBUCKET_REPO_SLUG:$BITBUCKET_COMMIT"

echo "pushing ${DOCKER_IMAGE_NAME} on GCR"
docker push ${DOCKER_IMAGE_NAME}

cat "./deployment/deployment.yaml" | sed "s~{{IMAGE_NAME}}~$DOCKER_IMAGE_NAME~g" | kubectl apply -f -

cat "./deployment/service.yaml" | kubectl apply -f -
