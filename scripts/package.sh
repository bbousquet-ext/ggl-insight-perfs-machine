#!/usr/bin/env bash
CUR_DIR=$(pwd)

mkdir dag_content
cd dag_content
cp -r ../../orchestration .
mv ./orchestration/critizr/dag_train_critizr_classification.py .
mv ./orchestration/critizr/dag_predict_critizr_classification.py .
zip -r ../dag_critizr_train_classification.zip *


gsutil cp ../dag_critizr_train_classification.zip gs://europe-west1-ggl-insight-pe-e7abaa75-bucket/dags
rm -f ../dag_critizr_train_classification.zip

gsutil cp ../../schemas/* gs://europe-west1-ggl-insight-pe-e7abaa75-bucket/data/schemas/

cd ..
rm -rf dag_content

cd ..
tar czvf ../ggl-insight-perfs-machine.tar.gz *
gsutil cp ../ggl-insight-perfs-machine.tar.gz gs://machine-critizr/artifacts/
rm -f ../ggl-insight-perfs-machine.tar.gz
